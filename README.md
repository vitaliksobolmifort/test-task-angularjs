# Test task based on the seed for AngularJS apps
To install:

```
npm install
```

To start:

```
npm start
```

And go to http://localhost:8000 or http://localhost:8000/index.html

Questions
1. Is it possible to create property without units? 
2. Are we have a limit on the maximum number of units in the property?
3. What about the maximum unit size? 
4. What about the minimum unit size?
5. Are we have validation for units with identical parameters in the same property? 
6. Should there be validation on the perimeter with the same address as already exists? 
7. What are the mandatory fields in the property?
8. What are the mandatory fields in units?
9. How do I get to the archive properties?
10. How do I unzip them?
11. Which browsers do we support?
12. Which devices?
13. What kind of OSs?
14. Do we need accessibility in the app?
15. Will it be in the public domain and should we think about SEO optimization?
16. Do we have performance requirements?
17. What will happen if properties a lot? Do we need pagination?
18. Do we need sorting?
19. Do we need filtering by each field?
20. Will there be pagination, sorting and filtering on the server side when there is a lot of properties?
21. What do we do if the property has a lot of units, show them all, or only part of them? 
22. Will the app have an analytics setup?
23. If it is a public site. Are we need to add a Cookie popup in case of the use of analytics to meet the requirements of the GDPR?
24. How large will be the app, because if it is large, you need to put in the architecture the possibility of scaling at once, such as modularity, pattern library, state manager, etc.
25. Will there be a test/acceptance/prod server for which different types of application builds (plane/minification/obfuscation) need to be configured?
26. What will be the deploying strategy?
27. Do we need to cover code with unit tests? If yes, what would be the quality gate? If yes, will we adjust the TDD/BDD?
28. Are we going to write the end-to-end tests?
29. Do we need to enter a code convention?
30. What secure requirements?
