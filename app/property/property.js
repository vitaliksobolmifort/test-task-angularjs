'use strict';

angular.module('myApp.property', ['ngRoute'])

.component('property', {
  templateUrl: 'property/property.html',
  controller: PropertyController,
  controllerAs: 'PropertyController'
})

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/property', {
    template: '<property></property>',
  });
}]);

PropertyController.$inject = ['$scope'];
function PropertyController($scope) {
  // Page load triggers Backend call that should return the following data
  $scope.properties = [
    {
      id: 1,
      name: 'Tower-Bower',
      description: 'Super cool and tall building',
      address: 'Baker Street 221a',
      isArchive: false,
      units: [
        {
          id: 101,
          number: 1,
          area: 60
        },
        {
          id: 102,
          number: 2,
          area: 150
        },
        {
          id: 103,
          number: 3,
          area: 92
        }
      ]
    },
    {
      id: 2,
      name: 'Building-on-a-lake',
      description: 'Super cool and tall building',
      address: 'Baker Street 221b',
      isArchive: false,
      units: [
        {
          id: 201,
          number: 1,
          area: 55
        },
        {
          id: 202,
          number: 2,
          area: 151
        },
        {
          id: 203,
          number: 3,
          area: 33
        }
      ]
    },
    {
      id: 3,
      name: 'Skyscraper',
      description: 'Super cool and tall building',
      address: 'Baker Street 221c',
      isArchive: true,
      units: [
        {
          id: 301,
          number: 1,
          area: 30
        },
        {
          id: 302,
          number: 2,
          area: 150
        },
        {
          id: 303,
          number: 3,
          area: 200
        }
      ]
    },
    {
      id: 4,
      name: 'Undergrounder',
      description: 'Super cool and tall building',
      address: 'Baker Street 221d',
      isArchive: false,
      units: [
        {
          id: 401,
          number: 1,
          area: 70
        },
        {
          id: 402,
          number: 2,
          area: 52
        },
        {
          id: 403,
          number: 3,
          area: 41
        }
      ]
    }
  ];

  /**
   * Opens modal for Unit editing
   * @param unit
   */

  $scope.onOpenModal = function (unit) {
    $scope.unitToEdit = unit;
    $scope.isSaved = false;
  };

  /**
   * Makes a call to backend to save a Unit into DB
   * @param unit
   */

  $scope.saveUnit = function (unit) {
    // Backend call to save the Unit
    $scope.isSaved = true;
  };

  $scope.changeArchiveStatus = function (property) {
    property.isArchive = !property.isArchive;
  };

}
