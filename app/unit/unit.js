'use strict';

angular.module('myApp.unit', ['ngRoute'])

.component('unit', {
    templateUrl: 'unit/unit.html',
    controller: UnitController,
    controllerAs: 'UnitController'
})

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/property/add', {
            template: '<unit></unit>',
            isEdit: false
        })
        .when('/property/edit', {
            template: '<unit></unit>',
            isEdit: true
        });
}]);

UnitController.$inject = ['$scope', '$route'];
function UnitController($scope, $route) {
    // Using the same module to edit or create unit
    $scope.isEdit = $route.current.$$route.isEdit;

    if ($scope.isEdit) {
        //Do a backend call to retrieve the latest Property data
        $scope.property = {
            id: 1,
            name: 'Tower-Bower',
            description: 'Super cool and tall building',
            address: 'Baker Street 221a',
            units: [
                {
                    id: 101,
                    number: 1,
                    area: 60
                },
                {
                    id: 102,
                    number: 2,
                    area: 150
                },
                {
                    id: 103,
                    number: 3,
                    area: 92
                }
            ]
        };
    } else {
        // Create a new Property dataset
        $scope.property = {
            id: Math.floor(Math.random() * 100),
            name: '',
            description: '',
            address: '',
            units: [
                {
                    id: Math.floor(Math.random() * 100),
                    number: null,
                    area: null
                }
            ]
        };
    }


    /**
     * Adds a new unit to Property form
     * @param $event
     */
    $scope.addUnit = function ($event) {
        var unitTemplate = {
            id: Math.floor(Math.random() * 100),
            number: null,
            area: null
        };

        $event.preventDefault();
        $scope.property.units.push(unitTemplate);
    };

    /**
     * Removes a last unit from Property form
     * @param $event
     */
    $scope.removeUnit = function ($event) {
        $event.preventDefault();
        $scope.property.units.pop();
    };

    /**
     * Makes a call to backend to save a new Property and it's Units into DB
     * @param {boolean} isValid
     */
    $scope.saveProperty = function (isValid) {
        // Backend call to save the property with error handling
        if (isValid) {
            $scope.isSaved = true;
            // Log into console so the task can be verified easily
            console.log($scope.property);
        }
    };

    /**
     * Makes a call to backend to delete a Property and it's Units from DB
     */
    $scope.deleteProperty = function () {
        // Backend call to delete the property with error handling
        $scope.actionIsDone = true;
        // Log into console so the task can be verified easily
        console.log($scope.property);
    };
}



